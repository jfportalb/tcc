#!/usr/bin/python
from typing import Set, Dict, Tuple, List
from race_detection import main

if __name__ == "__main__":
    asserts: List[Tuple[str, Set[str], Set[str]]] = [
        ["exemplos/tcc1", set(), set()],
        ["exemplos/tcc2", {"var"}, {"var"}],
        ["exemplos/tcc3", {"var"}, set()],
        ["exemplos/tcc4", {"var"}, set()],
        ["lab4/at3", set(), set()],
        ["lab4/atividade3Concorrente", set(), set()],
        ["lab4/calculaPi", set(), set()],
        ["lab4/MT_calcula_pi", set(), set()],
        ["lab4/pi_thread", {"pi"}, {"pi"}],
        ["lab4/piconc", set(), set()],
        ["trab1/main", {"matriz", "soma", "maiorValor", "maiorColuna", "maiorQuadrante"}, set()],
        ["trab1/main2", set(), set()],
        ["trab1/main3", {"soma", "maior_elemento", "maior_coluna", "maior_quadrante"}, set()],
        ["trab1/mainconc", {"MaiorElemento", "m1", "indiceLinha", "indiceColuna", "MaiorcolunaFinal",
                            "SomaTodosElementosFinal"}, set()],
        ["trab1/principal", {"numeroColuna", "tempo", "maiorElemento", "maiorQuadrante", "maiorQuadranteLinha",
                             "maiorQuadranteColuna", "somaTotal"}, set()],
        ["0", {"var"}, set()],
        ["1", set(), set()],
        ["2", set(), set()],
        ["3", {"var"}, {"var"}],
        ["4", set(), set()],
        ["5", {"var"}, {"var"}],
        ["6", set(), set()],
        ["7", {"var"}, {"var"}],
        ["8", {"var"}, {"var"}],
        ["9", {"var"}, {"var"}],
        ["10", {"var"}, {"var"}],
        ["11", {"var"}, {"var"}],
        ["12", {"var"}, {"var"}],
        ["13", set(), set()],
        ["14", {"var", "gg"}, {"var", "gg"}],
        ["15", {"gg"}, {"gg"}],
        ["16", {"var"}, {"var"}],
    ]
    for name, expected, real in asserts:
        s = set([k for k, v in main("tests/" + name + ".c")[1]["main"].variables.items() if v.racing])
        if len(s.union(expected) - (s & expected)) > 0:
            print()
            print("Test " + name + " failed")
            print("Expected: " + str(expected))
            print("Result: " + str(s))
        if len(s.union(real) - (s & real)) > 0:
            print()
            print("Test " + name + " had false positives/negatives")
            print("Real: " + str(real))
            print("Result: " + str(s))
