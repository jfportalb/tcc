#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
/* File:     timer.h
 *
 * Purpose:  Define a macro that returns the number of seconds that 
 *           have elapsed since some point in the past.  The timer
 *           should return times with microsecond accuracy.
 *
 * Note:     The argument passed to the GET_TIME macro should be
 *           a double, *not* a pointer to a double.
 *
 * Example:  
 *    #include "timer.h"
 *    . . .
 *    double start, finish, elapsed;
 *    . . .
 *    GET_TIME(start);
 *    . . .
 *    Code to be timed
 *    . . .
 *    GET_TIME(finish);
 *    elapsed = finish - start;
 *    printf("The code to be timed took %e seconds\n", elapsed);
 *
 * IPP:  Section 3.6.1 (pp. 121 and ff.) and Section 6.1.2 (pp. 273 and ff.)
 * Author:  Peter Pacheco
 */
#ifndef _TIMER_H_
#define _TIMER_H_

#include <sys/time.h>

/* The argument now should be a double (not a pointer to a double) */
#define GET_TIME(now) { \
   struct timeval t; \
   gettimeofday(&t, NULL); \
   now = t.tv_sec + t.tv_usec/1000000.0; \
}

#endif
#ifndef _EXECUTATAREFASCONC_H
#define _EXECUTATAREFASCONC_H 1

#define NTHREADS  4


float **m1;//Matrizes que alocarão os elementos declarados globalmente
float dimensaodaMatriz;
pthread_mutex_t mutex1; //variavel de lock para exclusao mutua
pthread_mutex_t mutex2; //variavel de lock para exclusao mutua
pthread_mutex_t mutex3; //variavel de lock para exclusao mutua
pthread_mutex_t mutex4; //variavel de lock para exclusao mutua

float MaiorElemento = 0.0;//Variável que guardará o maior elemento da matriz
float MaiorcolunaFinal = 0.0;//Variável que guarda o valor da maior soma de colunas, de todas as colunas da matriz
float SomaTodosElementosFinal = 0.0;
int indiceLinha;//Variável que guardará a linha em que o subquadrante de maior soma se encontra
int indiceColuna;//Variável que guardará a coluna em que o subquadrante de maior soma se encontra
float maiorQuadrante = 0.0;//Variável que guarda a maior soma dentre todos os quadrantes


//Função responsável por calcular o maior elemento da matriz
void *calculaMaiorElementodaMatriz(void *tid)
{
    int i,j; //Variáveis auxiliares
    int threadId = *(int *)tid;//Argumento passado pela pthread_create recebendo o typecast de int
    float maiorelemlocal = 0.0;

    for (i = threadId; i < dimensaodaMatriz; i+=NTHREADS)
    {
        for (j = 0; j < dimensaodaMatriz; j++)
        {
            if(m1[i][j] > maiorelemlocal)
            {
                maiorelemlocal = m1[i][j];
            }
        }
    }
    pthread_mutex_lock(&mutex1);
    if(maiorelemlocal>MaiorElemento)
    {
        MaiorElemento = maiorelemlocal;
    }
    pthread_mutex_unlock(&mutex1);
    pthread_exit(NULL);
}

//Função responsável por realizar a soma de cada coluna e retorna o resultado da maior soma
void *maiorSomaColuna(void *tid)
{
    int i,j;//Variáveis auxiliares
    int threadId = *(int *)tid;//Argumento passado pela pthread_create recebendo o typecast de int
    float SomaColuna= 0.0;

    for (j = threadId; j < dimensaodaMatriz; j+=NTHREADS)
    {
        for (i = 0; i < dimensaodaMatriz; i++)
        {
            SomaColuna= SomaColuna + m1[i][j];
        }
        pthread_mutex_lock(&mutex2);
        if(SomaColuna>MaiorcolunaFinal)
        {
            MaiorcolunaFinal = SomaColuna;
        }
        pthread_mutex_unlock(&mutex2);
        SomaColuna = 0.0;
    }
    pthread_exit(NULL);
}

//Função responsável por somar todos os elementos da matriz
void *calculaSomaElementosdaMatriz(void *tid)
{
    int i,j;//Variáveis auxiliares
    int threadId = *(int *)tid;//Argumento passado pela pthread_create recebendo o typecast de int
    float somaelemlocal = 0.0;

    for (i = threadId; i < dimensaodaMatriz; i+=NTHREADS)
    {
        for (j = 0; j < dimensaodaMatriz; j++)
        {
            somaelemlocal+=m1[i][j];
        }
    }
    pthread_mutex_lock(&mutex3);
    SomaTodosElementosFinal+=somaelemlocal;
    pthread_mutex_unlock(&mutex3);
    pthread_exit(NULL);
}

//Função responsável por realizar somas das submatrizes de dimensão 2x2 e retornar a posição da linha e coluna da maior soma das submatrizes.
void *calculaSomaSubMatrizesdeOrdem2(void *tid)
{
    int i,j;//Variáveis auxiliares
    int threadId = *(int *)tid;//Argumento passado pela pthread_create recebendo o typecast de int
    float somaQuadrante;

    for (i = threadId; i < dimensaodaMatriz; i+=NTHREADS)
    {
        for (j = 0; j < dimensaodaMatriz; j++)
        {
            if(((j+1) < dimensaodaMatriz)&&((i+1) < dimensaodaMatriz))
            {
                somaQuadrante = m1[i][j] + m1[i][j+1] + m1[i+1][j] + m1[i+1][j+1];

                //--entrada na SC
                pthread_mutex_lock(&mutex4);
                if(somaQuadrante > maiorQuadrante)
                {
                    maiorQuadrante = somaQuadrante;
                    indiceLinha = i;
                    indiceColuna = j;
                }
                pthread_mutex_unlock(&mutex4);
                // -- Saida na SC
            }
        }
    }
    pthread_exit(NULL);
}



#endif // _EXECUTATAREFASCONC_H
#ifndef _INITICONCORRENTE_H
#define _INITICONCORRENTE_H 1


int *le_matriz();
float **aloca(float lin, float col);
int escreve(float lin, float col, float **m);
float **libera(float lin, float **m);

//Função responsável por ler a matriz
int *le_matriz()
{
    FILE  *fr;
    int i,j;

    fr = fopen ("A2048x2048.txt", "r");

    if (fr == NULL)
    {
        perror("Não foi possível abrir o arquivo.\n");
        exit(-1);  /* Abandona o programa */
    }

    fscanf(fr, "%f",&dimensaodaMatriz);
    m1 = aloca(dimensaodaMatriz,dimensaodaMatriz);

    for (i=0; i<dimensaodaMatriz; i++)
    {
        for(j=0; j<dimensaodaMatriz; j++)
        {
            fscanf(fr,"%f",&(m1[i][j]));
        }
    }

    fclose(fr);
}

//Função responsável por alocar memória para a matriz
float **aloca(float lin, float col)
{
    int i; /*Variavel auxiliar*/
    float **m;
    m = (float **) malloc(lin*sizeof(float *));
    if(m!=NULL)
    {
        for (i=0; i<lin; i++)
        {
            m[i] = (float *) calloc(col,sizeof(float));
            if(m[i]==NULL)
            {
                printf("Memória Insuficiente.");
                exit(1);
            }
        }
    }
    else
    {
        printf("Memória Insuficiente.");
        exit(1);
    }
    return m;
}


//Função responsável por liberar memória
float **libera(float lin, float **m)
{
    int i; /*Variavel auxiliar*/
    if(m==NULL)
    {
        return(NULL);
    }

    for (i=0; i<lin; i++)
    {
        free(m[i]);
    }
    free(m);
}


#endif // _INITICONCORRENTE_H


#ifndef _TIMER_H_
#define _TIMER_H_

#include <sys/time.h>

/* The argument now should be a double (not a pointer to a double) */
#define GET_TIME(now) { \
   struct timeval t; \
   gettimeofday(&t, NULL); \
   now = t.tv_sec + t.tv_usec/1000000.0; \
}

#endif

void *calculaMaiorElementodaMatriz(void *tid);
void *maiorSomaColuna(void *tid);
void *calculaSomaElementosdaMatriz(void *tid);
void *calculaSomaSubMatrizesdeOrdem2(void *tid);

//Função Principal
void main ()
{
    //Variáveis responsáveis por pegar o tempo final e inicial de cada ação
    double start;
    double finish;

    double elapsed_init;//Variável que guarda o valor do tempo de execução da alocação
    double elapsed_print;//Variável que guarda o tempo de printagem do resultado da multiplicação
    double elapsedExecutaTodaTarefa;//Variável que calcula o tempo total da função ExecutarTarefa()
    double tempototal = 0;//Variável que guarda o tempo total da ação


    pthread_t tid_sistema[NTHREADS]; //vetor de identificadores das threads no sistema
    int tid_programa[NTHREADS]; //vetor de identificadores das threads no programa
    pthread_mutex_init(&mutex1, NULL); //--inicializa o mutex (lock de exclusao mutua)
    pthread_mutex_init(&mutex2, NULL); //--inicializa o mutex (lock de exclusao mutua)
    pthread_mutex_init(&mutex3, NULL); //--inicializa o mutex (lock de exclusao mutua)
    pthread_mutex_init(&mutex4, NULL); //--inicializa o mutex (lock de exclusao mutua)
    int t,i,j; //variável contadora

    GET_TIME(start);

    //Preenchendo Matriz
    *le_matriz(m1);
    GET_TIME(finish);

    elapsed_init = finish - start;//Guarda o tempo de alocação e leitura dos elementos da matriz

    GET_TIME(start);

    //Aqui as threads são criadas e usadas para a função calculaMaiorElementodaMatriz
    for(t = 0; t < NTHREADS; t++)
    {
        tid_programa[t] = t;
        if (pthread_create(&tid_sistema[t], NULL,calculaMaiorElementodaMatriz, (void*)&tid_programa[t]))
        {
            printf("--ERRO: pthread_create()\n");
            exit(-1);
        }
    }

    //Loop que espera todas as threads terminarem da função acima
    for(t = 0; t < NTHREADS; t++)
    {
        if(pthread_join(tid_sistema[t], NULL))
        {
            printf("---ERRO: pthread_join()\n");
            exit(-1);
        }
    }

    //Aqui as threads são criadas e usadas para a função maiorSomaColuna
    for(t = 0; t < NTHREADS; t++)
    {
        tid_programa[t] = t;
        if (pthread_create(&tid_sistema[t], NULL,maiorSomaColuna, (void*)&tid_programa[t]))
        {
            printf("--ERRO: pthread_create()\n");
            exit(-1);
        }
    }

    //Loop que espera todas as threads terminarem da função acima
    for(t = 0; t < NTHREADS; t++)
    {
        if(pthread_join(tid_sistema[t], NULL))
        {
            printf("---ERRO: pthread_join()\n");
            exit(-1);
        }
    }

    //Aqui as threads são criadas e usadas para a função calculaSomaElementosdaMatriz
    for(t = 0; t < NTHREADS; t++)
    {
        tid_programa[t] = t;
        if (pthread_create(&tid_sistema[t], NULL,calculaSomaElementosdaMatriz, (void*)&tid_programa[t]))
        {
            printf("--ERRO: pthread_create()\n");
            exit(-1);
        }
    }

    //Loop que espera todas as threads terminarem da função acima
    for(t = 0; t < NTHREADS; t++)
    {
        if(pthread_join(tid_sistema[t], NULL))
        {
            printf("---ERRO: pthread_join()\n");
            exit(-1);
        }
    }

    //Aqui as threads são criadas e usadas para a função calculaSomaSubMatrizesdeOrdem2
    for(t = 0; t < NTHREADS; t++)
    {
        tid_programa[t] = t;
        if (pthread_create(&tid_sistema[t], NULL,calculaSomaSubMatrizesdeOrdem2, (void*)&tid_programa[t]))
        {
            printf("--ERRO: pthread_create()\n");
            exit(-1);
        }
    }

    //Loop que espera todas as threads terminarem
    for(t = 0; t < NTHREADS; t++)
    {
        if(pthread_join(tid_sistema[t], NULL))
        {
            printf("---ERRO: pthread_join()\n");
            exit(-1);
        }
    }
    GET_TIME(finish);

    elapsedExecutaTodaTarefa = finish - start;

    tempototal = elapsed_init + elapsed_print + elapsedExecutaTodaTarefa;

    //Apresenta o resultado da Operação que calcula o maior elemento da matriz
    printf("\n\nO maior elemento da matriz eh %f\n", MaiorElemento);

    //Apresenta o resultado da Operação que calcula a maior soma de colunas dentre todas as colunas da matriz
    printf("A soma da maior coluna eh %f\n",MaiorcolunaFinal);

    //Apresenta o resultado da Operação que soma todos os elementos da matriz
    printf("A soma de todos os elementos da matriz eh %f\n",SomaTodosElementosFinal);

    //Apresenta o resultado da Operação 4
    /*Aqui printamos na tela qual a POSIÇÃO i,j da submatriz que contem a maior soma dentro todos os quadrandes 2x2
    Essa posição é a posiçao do elemento que fica no lado superior esquerdo da submatriz 2x2 de maior soma.*/
    printf("A submatriz 2x2 que possui a maior soma entre todas as submatrizes é a matriz[%d][%d]\n\n", indiceLinha,indiceColuna);


    //Apresenta os tempos medidos
    printf("\nA inicialização levou %10lf segundos\n", elapsed_init);
    printf("O tempo de execução das 4 operações foi de %10lf segundos\n",elapsedExecutaTodaTarefa);
    printf("O tempo total do programa foi de %10lf segundos\n", tempototal);
    puts("\n");

    //libera memória
    pthread_mutex_destroy(&mutex1);
    pthread_mutex_destroy(&mutex2);
    pthread_mutex_destroy(&mutex3);
    pthread_mutex_destroy(&mutex4);
    m1 = libera(dimensaodaMatriz,m1);

    exit(0);
}
