#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
/* File:     timer.h
 *
 * Purpose:  Define a macro that returns the number of seconds that 
 *           have elapsed since some point in the past.  The timer
 *           should return times with microsecond accuracy.
 *
 * Note:     The argument passed to the GET_TIME macro should be
 *           a double, *not* a pointer to a double.
 *
 * Example:  
 *    #include "timer.h"
 *    . . .
 *    double start, finish, elapsed;
 *    . . .
 *    GET_TIME(start);
 *    . . .
 *    Code to be timed
 *    . . .
 *    GET_TIME(finish);
 *    elapsed = finish - start;
 *    printf("The code to be timed took %e seconds\n", elapsed);
 *
 * IPP:  Section 3.6.1 (pp. 121 and ff.) and Section 6.1.2 (pp. 273 and ff.)
 * Author:  Peter Pacheco
 */
#ifndef _TIMER_H_
#define _TIMER_H_

#include <sys/time.h>

/* The argument now should be a double (not a pointer to a double) */
#define GET_TIME(now) { \
   struct timeval t; \
   gettimeofday(&t, NULL); \
   now = t.tv_sec + t.tv_usec/1000000.0; \
}

#endif

extern float **matriz;
extern int tam;

float **aloca_matriz();

void imprime_matriz();

void le_matriz();

float **aloca_matriz() {
	int i;
	
	float **m = malloc(tam * sizeof(float *));
	
	for(i = 0; i < tam; i++)
		m[i] = malloc(tam * sizeof(float));
	
	return m;
}

void imprime_matriz() {
	int i, j;
		
	for(i = 0; i < tam; i++) {
		for(j = 0; j < tam; j++) {
			printf("%5.1lf", matriz[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void le_matriz() {

	int i, j;
	
	for(i = 0; i < tam; i++) {
		for(j = 0; j < tam; j++) {
			scanf("%f", &matriz[i][j]);
		}
	}
}

void libera_matriz() {
    int i;
	for(i = 0; i < tam; i++) {
		free(matriz[i]);
	}
	
	free(matriz);
}

extern float **matriz;

typedef struct {
	int x;
	int y;
} ponto;

float max(float a, float b);


float soma_coluna(int col);
int   soma_coluna_max();

float soma_quadrante(ponto p);
ponto soma_quadrante_max();

float elemento_max();

float soma_total();


float max(float a, float b) {
	return (a > b) ? a : b;
}

float soma_coluna(int col) {
	int i;
	float r = 0;
	
	for(i = 0; i < tam; i++)
		r += matriz[i][col];
	
	return r;
}

int soma_coluna_max() {
	int i;
	
	float max_soma = soma_coluna(0);
	float max_col = 0;
	
	for(i = 1; i < tam; i++) {
		float soma = soma_coluna(i);
		if(soma >= max_soma) {
			max_soma = soma;
			max_col = i;
		}
	}
	
	return max_col;
	
}


float elemento_max() {
	int i, j;
	float e = matriz[0][0];
	
	for(i = 0; i < tam; i++)
		for(j = 0; j < tam; j++)
			e = max(e, matriz[i][j]);
	
	return e;
}


float soma_total() {
	int i, j;
	float e = 0;
	
	for(i = 0; i < tam; i++)
		for(j = 0; j < tam; j++)
			e += matriz[i][j];
	
	return e;
}


float soma_quadrante(ponto p) {
	int i, j;
	float e = 0;
	
	for(i = p.x; i < p.x+2; i++) {
		for(j = p.y; j < p.y+2; j++) {
			e += matriz[i][j];
		}
	}
	
	return e;
}

ponto soma_quadrante_max() {
	int i, j;
	
	float soma_max = soma_quadrante((ponto){0, 0});
	float soma;
	ponto quadrante = {0,0};
	
	for(i = 0; i <= tam-2; i++) {
		for(j = 0; j <= tam-2; j++) {
			float soma = soma_quadrante((ponto){i, j});
			
			if(soma >= soma_max) {
				soma_max = soma;
				quadrante = (ponto){i, j};
			}
		}
	}
	
	return quadrante;
}
extern int num_threads;

void *concorrente(void *arg);
void concorrente_inicio();
void concorrente_final();

void soma_quad_concorrente(int thread);
void soma_col_concorrente(int thread);

ponto quad_max_coluna(int col);
void quad_max_concorrente(int thread);

void elem_max_concorrente(int thread);
float elem_max_linha(int lin);


float *somas_col;
int *col_max_thread;
float *col_max_thread_soma;

float *somas_quad;
ponto *quad_max_thread;
float *quad_max_thread_soma;

float **somas_quadrantes;

float *elem_max_thread;

extern int num_threads;


void *concorrente(void *arg) {
	int thread = *(int*) arg;
	free(arg);
	
	soma_col_concorrente(thread);
	quad_max_concorrente(thread);
	elem_max_concorrente(thread);
	
	pthread_exit(NULL);
}


void concorrente_inicio() {
	
	int i;
	
	somas_col = malloc(tam * sizeof(float));
	col_max_thread = malloc(num_threads * sizeof(int));
	col_max_thread_soma = malloc(num_threads * sizeof(float));
	
	somas_quadrantes = malloc(tam * sizeof(float *));
	for(i = 0; i < tam; i++)
		somas_quadrantes[i] = malloc(tam * sizeof(float));
	
	quad_max_thread = malloc(num_threads * sizeof(ponto));
	quad_max_thread_soma = malloc(num_threads * sizeof(float));
	
	elem_max_thread = malloc(num_threads * sizeof(float));
}

void concorrente_final() {
	int i;

	
	int col_max = col_max_thread[0];
	float col_max_soma =col_max_thread_soma[0];
	
	for(i = 1; i < num_threads; i++) {
	
		if(col_max_thread_soma[i] > col_max_soma) {
			col_max = col_max_thread[i];
			col_max_soma =col_max_thread_soma[i];
		}
	}
	
	printf("Coluna com a maior soma total: %d\n", col_max);
	
	
	
	float maior = elem_max_thread[0];

	for(i = 1; i < num_threads; i++)
		maior = max(maior, elem_max_thread[i]);
	
	printf("Maior elemento (valor) da matriz: %.1f\n", maior);
		
	
	
	ponto quad_max = quad_max_thread[0];
	float quad_max_soma = quad_max_thread_soma[0];
	
	for(i = 1; i < num_threads; i++) {
	
		if(quad_max_thread_soma[i] > quad_max_soma) {
			quad_max = quad_max_thread[i];
			quad_max_soma = quad_max_thread_soma[i];
		}
	}
	
	printf("Maior quadrante 2x2: (%d,%d)\n", quad_max.x, quad_max.y);
	
	

	float total = 0;

	for(i = 0; i < tam; i++)
		total += somas_col[i];
	
	printf("Soma de todos os elementos da matriz: %.1f\n", total);
}



void soma_col_concorrente(int thread) {

	/* já acha a col máxima da thread atual */
	int col_max = thread;
	float col_max_soma = somas_col[thread] = soma_coluna(thread);
	
	int i;
	
	for(i = thread+num_threads; i < tam; i += num_threads) {
		somas_col[i] = soma_coluna(i);
		
		if(somas_col[i] > col_max_soma) {
			col_max = i;
			col_max_soma = somas_col[i];
		}
	}
	
	
	col_max_thread[thread] = col_max; // coluna máxima da thread atual
	col_max_thread_soma[thread] = col_max_soma; // soma da coluna máxima da thread atual
}



void quad_max_concorrente(int thread) {

	int i;

	
	ponto quad_max = quad_max_coluna(thread);
	float quad_max_soma = soma_quadrante(quad_max);
	
	for(i = thread+num_threads; i < tam-1; i += num_threads) {
		ponto quad = quad_max_coluna(i); // quadrante max da coluna i
		float soma = soma_quadrante(quad);
		
		if(soma > quad_max_soma) {
			quad_max = quad;
			quad_max_soma = soma;
		}
	}
	
	quad_max_thread[thread] = quad_max;
	quad_max_thread_soma[thread] = quad_max_soma;
}


ponto quad_max_coluna(int col) {
	
	int i;
	
	ponto quad_max = (ponto){0, col};
	float quad_max_soma = soma_quadrante(quad_max);

	for(i = 1; i < tam-1; i++) {
		ponto quad = (ponto){i, col};
		float soma = soma_quadrante(quad);
		
		if(soma > quad_max_soma) {
			quad_max = quad;
			quad_max_soma = soma;
		}
	}
	
	return quad_max;
}

void elem_max_concorrente(int thread) {
	int i;
	
	float maior = elem_max_linha(thread);
	
	for(i = thread+num_threads; i < tam; i += num_threads) {
		maior = max(maior, elem_max_linha(i));
	}
	
	elem_max_thread[thread] = maior;
}

float elem_max_linha(int lin) {
	int i;

	float maior = matriz[lin][0];
	
	for(i = 1; i < tam; i++)
		maior = max(maior, matriz[lin][i]);
	
	return maior;
}

float **matriz;
int tam;
int num_threads;

int main(int argc, char *argv[]) {
	
	if(argc < 2) {
		printf("Uso: %s NUM_THREADS < matriz.txt\n", argv[0]);
		exit(0);
	}
	
	int i, j;
	double inicio, fim;
	num_threads = atoi(argv[1]);
	
	if(num_threads < 1) {
		printf("O número de threads não pode ser 0!");
		exit(1);
	}
	
	
	printf("+------------------------------------------+\n");
	
	GET_TIME(inicio);
	
	scanf("%d", &tam);
	
	matriz = aloca_matriz();
	le_matriz();
	
	GET_TIME(fim);
	printf("| Leitura da matriz... %9.6lfs          |\n", fim-inicio);
	
	if(num_threads == 1) { // processamento sequencial
		
		printf("| Processamento sequencial                 |\n");
		printf("+------------------------------------------+\n\n");
		GET_TIME(inicio);
	
		ponto maior_quadrante = soma_quadrante_max();
	
		printf("Coluna com a maior soma total: %d\n", soma_coluna_max());
		printf("Maior elemento (valor) da matriz: %.1f\n", elemento_max());
		printf("Maior quadrante 2x2: (%d,%d)\n", maior_quadrante.x, maior_quadrante.y);
		printf("Soma de todos os elementos da matriz: %.1f\n", soma_total());
	
		GET_TIME(fim);
		printf("\nTempo de processamento: %lfs\n\n", fim-inicio);
	
	}
	
	else { // processamento concorrente
	
		printf("| Processamento concorrente com %2d threads |\n", num_threads);
		printf("+------------------------------------------+\n\n");
		GET_TIME(inicio);
	
		concorrente_inicio(); // alocar vetores dinamicamente
	
		pthread_t *tid = malloc(num_threads * sizeof(pthread_t));;

		for(i = 0; i < num_threads; i++) {
			int *t = malloc(sizeof(int));
			*t = i;
			if(pthread_create(&tid[i], NULL, concorrente, (void*) t)) {
				printf("erro na criação\n"); exit(-1);
			}
		}
	
		for(i = 0; i < num_threads; i++) {
			if(pthread_join(tid[i], NULL)) {
				printf("erro na espera\n"); exit(-1);
			}
		}
	
		concorrente_final(); // parte não concorrente dos cálculos e impressao dos resultados
	
		GET_TIME(fim);
		printf("\nTempo de processamento: %lfs\n\n", fim-inicio);
	
		pthread_exit(NULL);
	}
	
	
	
	return 0;
}
