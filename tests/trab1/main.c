
#include <stdio.h>
#include <stdlib.h>

/* File:     timer.h
 *
 * Purpose:  Define a macro that returns the number of seconds that 
 *           have elapsed since some point in the past.  The timer
 *           should return times with microsecond accuracy.
 *
 * Note:     The argument passed to the GET_TIME macro should be
 *           a double, *not* a pointer to a double.
 *
 * Example:  
 *    #include "timer.h"
 *    . . .
 *    double start, finish, elapsed;
 *    . . .
 *    GET_TIME(start);
 *    . . .
 *    Code to be timed
 *    . . .
 *    GET_TIME(finish);
 *    elapsed = finish - start;
 *    printf("The code to be timed took %e seconds\n", elapsed);
 *
 * IPP:  Section 3.6.1 (pp. 121 and ff.) and Section 6.1.2 (pp. 273 and ff.)
 * Author:  Peter Pacheco
 */
#ifndef _TIMER_H_
#define _TIMER_H_

#include <sys/time.h>

/* The argument now should be a double (not a pointer to a double) */
#define GET_TIME(now) { \
   struct timeval t; \
   gettimeofday(&t, NULL); \
   now = t.tv_sec + t.tv_usec/1000000.0; \
}

#endif

#define TIMER 1



int recebeEntrada();
void imprimeResultado();
void calculaResultado(); //Esta função será implementada pelos outros arquivos.

#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

#define NTHREADS 4

extern int dim;
extern float * matriz;

extern int maiorQuadrante;
float maiorQuadranteValor;
extern float maiorValor;
extern int maiorColuna;
float maiorColunaValor;
extern float soma;

pthread_mutex_t somaMutex;
pthread_mutex_t maiorValorMutex;
pthread_mutex_t maiorColunaMutex;
pthread_mutex_t maiorQuadranteMutex;

void calculoDaColunaFinal(){
	int j = dim - 1;
	int i;
	float somaColunaTemp = 0;
	
	for (i = 0; i < dim; i++){
		pthread_mutex_lock(&maiorValorMutex);
		if (matriz[i*dim + j] > maiorValor){
			maiorValor = matriz[i*dim + j];
		}
		pthread_mutex_unlock(&maiorValorMutex);
		
		somaColunaTemp += matriz[i*dim + j];
		
		
		
		pthread_mutex_lock(&somaMutex);
		soma+= matriz[i*dim + j];
		pthread_mutex_unlock(&somaMutex);
		
	}
	pthread_mutex_lock(&maiorColunaMutex);
	if(somaColunaTemp > maiorColunaValor){
		maiorColunaValor = somaColunaTemp;
		maiorColuna = j;
	}
	pthread_mutex_unlock(&maiorColunaMutex);
	
	return;
}


void calculaQuadrante(int i, int j, float * ptrColunaTemp){
	float ValorDoQuadranteAtual = 0;
	ValorDoQuadranteAtual += matriz[i*dim + j] + matriz[(i+1)*dim +j] + matriz[i*dim + j + 1] + matriz[(i+1)*dim+j+1];
	pthread_mutex_lock(&maiorQuadranteMutex);
	if (ValorDoQuadranteAtual > maiorQuadranteValor){
		maiorQuadranteValor = ValorDoQuadranteAtual;
		maiorQuadrante = i*dim + j;
	}
	pthread_mutex_unlock(&maiorQuadranteMutex);
	
	pthread_mutex_lock(&somaMutex);
	soma+= matriz[i*dim + j];
	pthread_mutex_unlock(&somaMutex);
	
	*(ptrColunaTemp) += matriz[i*dim + j];
	
	pthread_mutex_lock(&maiorValorMutex);
	if (matriz[i*dim + j] > maiorValor){
		maiorValor = matriz[i*dim + j];
	}
	pthread_mutex_unlock(&maiorValorMutex);
	
}

void * calculoNaSubmatriz(void * voidIntervaloDeColunas){
	float somaColunaTemp;
	int * intervalo = (int *) voidIntervaloDeColunas;
	int i,j;
		for (j = intervalo[0]; j < intervalo[1]; j++){
		somaColunaTemp = 0;
		for (i = 0; i < dim-1; i++){
			calculaQuadrante(i, j, &somaColunaTemp);	
		}
		somaColunaTemp += matriz[(dim-1)*dim + j];
		
		pthread_mutex_lock(&somaMutex);
		soma += matriz[(dim-1)*dim + j];
		pthread_mutex_unlock(&somaMutex);
		
		pthread_mutex_lock(&maiorColunaMutex);
		if(somaColunaTemp > maiorColunaValor){
			maiorColunaValor = somaColunaTemp;
			maiorColuna = j;
		}
		pthread_mutex_unlock(&maiorColunaMutex);
	}
	free(voidIntervaloDeColunas);
	pthread_exit(NULL);
	return NULL;
}

void calculaResultado(){	
	pthread_t tid[NTHREADS];
	int j, quo, res;
	int id = 0;
	int * argIntervalo;
	
	//Inicializacação dos locks
	pthread_mutex_init(&maiorQuadranteMutex, NULL);
	pthread_mutex_init(&maiorColunaMutex, NULL);
	pthread_mutex_init(&maiorValorMutex, NULL);
	pthread_mutex_init(&somaMutex, NULL);
	
	quo = (dim-1) / NTHREADS;
	res = (dim - 1) % NTHREADS;
	
	for (j = 0; j < res*(quo+1); j+= quo+1){
		argIntervalo = (int *) malloc(2*sizeof(int));
		argIntervalo[0] = j;
		argIntervalo[1] = j + quo + 1;
		pthread_create(tid+id, NULL, calculoNaSubmatriz, ((void *) argIntervalo) );
		id++;
	}
	
	
	for(; j < dim - 1; j+=quo){
		argIntervalo = (int *) malloc(2*sizeof(int));
		argIntervalo[0] = j;
		argIntervalo[1] = j + quo;
		pthread_create(tid+id, NULL, calculoNaSubmatriz, ((void *) argIntervalo) );
		id++;
	}
	
	calculoDaColunaFinal();
	
	for (id -= 1; id >= 0; id--) pthread_join(tid[id], NULL);
	
	pthread_mutex_destroy(&somaMutex);
	pthread_mutex_destroy(&maiorValorMutex);
	pthread_mutex_destroy(&maiorColunaMutex);
	pthread_mutex_destroy(&maiorQuadranteMutex);
	
	
	return;
}
int dim;
float * matriz;

int maiorQuadrante = 0; //O ponto (i,j) resultará em i*dim + j
float maiorValor = 0;
int maiorColuna = 0;
float soma = 0;

int main(){
	double entradaTempo, calculoTempo, impressaoTempo, ini, fim;
	if (TIMER) GET_TIME(ini);
	while (recebeEntrada() != EOF){
		if(TIMER){
			GET_TIME(fim);
			entradaTempo = fim - ini;
			GET_TIME(ini);
		}
		calculaResultado();
		if (TIMER) {
			GET_TIME(fim);
			calculoTempo = fim - ini;
			GET_TIME(ini);
		}
		imprimeResultado();
		if (TIMER) {
			GET_TIME(fim);
			impressaoTempo = fim - ini;
			printf("Tempo gasto:\n- Recebendo a entrada: %0.8f\n- Calculando as matrizes: %0.8f\n- Imprimindo o resultado: %0.8f\n\n", entradaTempo, calculoTempo, impressaoTempo);
			GET_TIME(ini);
		}
	}
	return 0;
}

int recebeEntrada(){
	int retorno;
	retorno = scanf("%i", &dim);
	matriz = (float *) malloc(dim*dim*sizeof(float));
	int i;
	for (i = 0; i< dim*dim; i++){
		scanf("%f", matriz + i);
	}
	return retorno;
}

void imprimeResultado(){
	printf("Coluna com a maior soma total: %i\nMaior elemento (valor) da matriz: %f\nMaior quadrante 2x2: (%i,%i)\nSoma de todos os elementos da matriz: %f\n",   
	maiorColuna, maiorValor, (int) maiorQuadrante/dim, maiorQuadrante % dim, soma);
	return;
}