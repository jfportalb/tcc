#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
/* File:     timer.h
 *
 * Purpose:  Define a macro that returns the number of seconds that 
 *           have elapsed since some point in the past.  The timer
 *           should return times with microsecond accuracy.
 *
 * Note:     The argument passed to the GET_TIME macro should be
 *           a double, *not* a pointer to a double.
 *
 * Example:  
 *    #include "timer.h"
 *    . . .
 *    double start, finish, elapsed;
 *    . . .
 *    GET_TIME(start);
 *    . . .
 *    Code to be timed
 *    . . .
 *    GET_TIME(finish);
 *    elapsed = finish - start;
 *    printf("The code to be timed took %e seconds\n", elapsed);
 *
 * IPP:  Section 3.6.1 (pp. 121 and ff.) and Section 6.1.2 (pp. 273 and ff.)
 * Author:  Peter Pacheco
 */
#ifndef _TIMER_H_
#define _TIMER_H_

#include <sys/time.h>

/* The argument now should be a double (not a pointer to a double) */
#define GET_TIME(now) { \
   struct timeval t; \
   gettimeofday(&t, NULL); \
   now = t.tv_sec + t.tv_usec/1000000.0; \
}

#endif
#define NUMTHREADS 4

pthread_mutex_t x_mutex;
pthread_cond_t x_cond;

volatile double maiorColuna = 0; volatile int numeroColuna = 0;
volatile double maiorElemento = 0;
volatile double somaTotal = 0;
volatile double maiorQuadrante = 0; volatile int maiorQuadranteLinha = 0, maiorQuadranteColuna = 0;
volatile double tempo = 0;

int tamanho;
float **matriz;

void *maiorColunaConc(void *tid) {
	double inicio, fim;
	GET_TIME(inicio);
	double aux = 0;
	int i, j;
	int thread_id = *(int *)tid;
	for(i = thread_id; i < tamanho; i += NUMTHREADS) {
		for(j = 0; j < tamanho; j ++) {
			aux += matriz[j][i];
		}
		pthread_mutex_lock(&x_mutex);
		if( aux > maiorColuna) {
			maiorColuna = aux;
			numeroColuna = i;
		}
		pthread_mutex_unlock(&x_mutex);
		aux = 0;
	}

	GET_TIME(fim);
	if((fim - inicio) > tempo) {
		tempo = fim - inicio;
	}
	pthread_exit(NULL);
}

void *maiorElementoConc(void *tid) {
	double inicio,fim;
	GET_TIME(inicio);
	int i, j;
	int thread_id = *(int *)tid;
	for(i = thread_id; i < tamanho; i += NUMTHREADS) {
		for(j = 0; j < tamanho; j++) {
			if (matriz[i][j] > maiorElemento) {
				maiorElemento = matriz[i][j];
			}
		}
	}
	GET_TIME(fim);
	if((fim - inicio) > tempo) {
		tempo = fim - inicio;
	} 
	pthread_exit(NULL);
}

void* maiorQuadranteConc(void *tid) {
	double inicio, fim;
	GET_TIME(inicio);
	double aux = 0;
	int i, j;
	int thread_id = *(int *)tid;
	
	for(i = thread_id; i < (tamanho - 1); i += NUMTHREADS) {
		for(j = 0; j < (tamanho - 1); j++) {
			aux += matriz[i][j] + matriz[i][j+1] + matriz[i+1][j] + matriz[i+1][j+1];
			if(aux > maiorQuadrante) {
				maiorQuadrante = aux;
				maiorQuadranteLinha = i;
				maiorQuadranteColuna = j;
			}
			aux = 0;
		}
	}

	GET_TIME(fim);
	if((fim - inicio) > tempo) {
		tempo = fim - inicio;
	} 
}

void *somaTodosElementosConc(void *tid) {
	double inicio, fim;
	GET_TIME(inicio);
	int i, j;
	int thread_id = *(int *)tid;
	for(i = thread_id; i < tamanho; i += NUMTHREADS) {
		for(j = 0; j < tamanho; j++) {
			//pthread_mutex_lock(&x_mutex);
			somaTotal += matriz[i][j]; //mutex nao deveria ser necessario aqui, mas esta ocorrendo um erro
			//pthread_mutex_unlock(&x_mutex); //mutex resolve, mas deixa muuuuito lento
		}                                 
	}
	GET_TIME(fim);
	if((fim - inicio) > tempo) {
		tempo = fim - inicio;
	} 

}

void executaConcorrentemente() {
	int i;
	int *t;
	double inicio,fim;

	pthread_t tid[NUMTHREADS];
	
	pthread_mutex_init(&x_mutex, NULL);
	pthread_cond_init(&x_cond,NULL);

/////////////////////////////////////////////////////////
	GET_TIME(inicio);
	for(i = 0; i < NUMTHREADS; i++) {
		t = malloc(sizeof(int));
		*t = i;
		pthread_create(&tid[i], NULL, *maiorColunaConc,(void *)t);
	}
	GET_TIME(fim);
	for(i = 0; i < NUMTHREADS; i++) {
		pthread_join(tid[i], NULL);
	}

	printf("---\nMaior coluna: %d\n", numeroColuna);
	printf("Tempo de execucao maiorColuna: %lf\n", tempo + fim - inicio);
	tempo = 0;

///////////////////////////////////////////////////////
	GET_TIME(inicio);
	for(i = 0; i < NUMTHREADS; i++) {
		t = malloc(sizeof(int));
		*t = i;
		pthread_create(&tid[i], NULL, *maiorElementoConc,(void *)t);
	}
	GET_TIME(fim);
	for(i = 0; i < NUMTHREADS; i++) {
		pthread_join(tid[i], NULL);
	}
	
	printf("---\nMaior elemento: %.2lf\n", maiorElemento);
	printf("Tempo de execucao maiorElemento: %lf\n",  tempo + fim - inicio);
	tempo = 0;

//////////////////////////////////////////////////////
	GET_TIME(inicio);
	for(i = 0; i < NUMTHREADS; i++) {
		t = malloc(sizeof(int));
		*t = i;
		pthread_create(&tid[i], NULL, *maiorQuadranteConc,(void *)t);
	}
	GET_TIME(fim);
	for(i = 0; i < NUMTHREADS; i++) {
		pthread_join(tid[i], NULL);
	}

	printf("---\nMaior quadrante 2x2: (%d, %d)\n", maiorQuadranteLinha, maiorQuadranteColuna);
	printf("Tempo de execucao maiorQuadrante: %lf\n",  tempo + fim - inicio);
	tempo = 0;

////////////////////////////////////////////////////
	GET_TIME(inicio);
	for(i = 0; i < NUMTHREADS; i++) {
		t = malloc(sizeof(int));
		*t = i;
		pthread_create(&tid[i], NULL, *somaTodosElementosConc,(void *)t);
	}
	GET_TIME(fim);
	for(i = 0; i < NUMTHREADS; i++) {
		pthread_join(tid[i], NULL);
	}

	printf("---\nSoma de todos elementos: %.2lf\n", somaTotal);
	printf("Tempo de execucao somaTodosElementos: %lf\n---\n",  tempo + fim - inicio);
	tempo = 0;

/////////////////////////////////////////////////////

	pthread_mutex_destroy(&x_mutex);	
	pthread_cond_destroy(&x_cond);
}


float **matriz;
int tamanho;

void iniciaMatriz() {
	printf("Tamanho?\n");
	scanf("%d", &tamanho);
	printf("Inicializando matriz...\n");
	
	int i,j;
	matriz = malloc(sizeof(float*)*tamanho);
	for(i = 0; i < tamanho; i++) {
		matriz[i] = malloc(sizeof(float)*tamanho);
	}

	FILE *file;
	file = fopen("entrada.txt", "r");

	for(i = 0; i < tamanho; i++){
		for(j = 0; j < tamanho; j++) {
			fscanf(file, "%f", &matriz[i][j]);
		}
	}

	fclose(file);

	/*for(i = 0; i < tamanho; i++){
		for(j = 0; j < tamanho; j++) {
			printf("%.2f ", matriz[i][j]);
		}
		printf("\n");
	}*/
}


int main () {
	double start,end;
	iniciaMatriz();
	printf("Executar sequencialmente ou concorrentemente? (S/C)\n");
	char input;
	scanf(" %c", &input);
	if(input == 's' || input == 'S') {
		GET_TIME(start);
//		executaSequencialmente(matriz,tamanho);
		GET_TIME(end);
		printf("Tempo de execucao sequencial: %.8lf\n", end - start);
	}
	if(input == 'c' || input == 'C') {
		GET_TIME(start);
		executaConcorrentemente();
		GET_TIME(end);
		printf("Tempo de execucao concorrente: %.8lf\n", end - start);
	}

	free(matriz);
	pthread_exit(NULL);
	return 0;
}
