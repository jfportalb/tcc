#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

/* File:     timer.h
 *
 * Purpose:  Define a macro that returns the number of seconds that 
 *           have elapsed since some point in the past.  The timer
 *           should return times with microsecond accuracy.
 *
 * Note:     The argument passed to the GET_TIME macro should be
 *           a double, *not* a pointer to a double.
 *
 * Example:  
 *    #include "timer.h"
 *    . . .
 *    double start, finish, elapsed;
 *    . . .
 *    GET_TIME(start);
 *    . . .
 *    Code to be timed
 *    . . .
 *    GET_TIME(finish);
 *    elapsed = finish - start;
 *    printf("The code to be timed took %e seconds\n", elapsed);
 *
 * IPP:  Section 3.6.1 (pp. 121 and ff.) and Section 6.1.2 (pp. 273 and ff.)
 * Author:  Peter Pacheco
 */
#ifndef _TIMER_H_
#define _TIMER_H_

#include <sys/time.h>

/* The argument now should be a double (not a pointer to a double) */
#define GET_TIME(now) { \
   struct timeval t; \
   gettimeofday(&t, NULL); \
   now = t.tv_sec + t.tv_usec/1000000.0; \
}

#endif
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#ifndef CONCORRENTE_H
#define CONCORRENTE_H

void concorrente();

void c_maior_soma();
void c_maior_elemento();
void c_maior_quadrante();
void c_soma_total();

void *t_maior_soma(void *thread_id);
void *t_maior_elemento(void *thread_id);
void *t_maior_quadrante(void *thread_id);
void *t_soma_total(void *thread_id);

#endif
extern float **matriz;
extern int ordem;

pthread_t *tid;
pthread_mutex_t mutex;
int *id;
int num_threads, lim_threads;

/* variaveis a serem acessadas pelas threads */
volatile float maximo, maximo_quad, soma, maior_elemento;
volatile int maior_coluna, maior_quadrante[2];

/* funcao principal de concorrente.c, chamada pela funcao main() do programa.Essa funcao pergunta ao usuario
quantas threads ele deseja usar, aloca memoria para estas threads e chama as funcoes que fazem as operacoes */
void concorrente() {

	printf("Quantas threads devo usar?");
	scanf("%d", &num_threads);

	if(num_threads > ordem) num_threads = ordem;

	tid = (pthread_t*) malloc(num_threads * sizeof(pthread_t));
	id =(int*) malloc(num_threads * sizeof(int));

	pthread_mutex_init(&mutex, NULL);

	c_maior_soma();
	c_maior_elemento();
	c_maior_quadrante();
	c_soma_total();

	free(tid);
	free(id);
}
/* as quatro funcoes: c_maior_soma(), c_maior_elemento(), c_maior_quadrante(), c_soma_total, criam as threads,
que realizaram as respectivas operacoes */
void c_maior_soma() {

	int i,j;

	for(j=0; j<ordem; j++) maximo+= matriz[1][j]; 
	for(i=0; i<num_threads; i++) {
		id[i] = i;
		pthread_create(&tid[i], NULL, t_maior_soma, (void*) &id[i]);
	}

	for(i=0; i<num_threads; i++) pthread_join(tid[i], NULL);

	printf("coluna com a maior soma total : %d\n", maior_coluna);

}

void c_maior_elemento(){ 

	int i;

	maior_elemento = matriz[0][0];
	
	for(i=0; i<num_threads; i++) {
		id[i] = i;
		pthread_create(&tid[i], NULL, t_maior_elemento, (void*) &id[i]);
	}

	for(i=0; i<num_threads; i++) pthread_join(tid[i], NULL);

	printf("Maior elemento(valor) da matriz: %.2f\n", maior_elemento);

}
void c_maior_quadrante(){ 

	int i;

	if(num_threads > ordem - 1) lim_threads = ordem - 1;
	else lim_threads = num_threads;

	maximo_quad = matriz[0][0] + matriz[0][1]+matriz[1][0]+matriz[1][1];

	for(i=0; i<lim_threads; i++) {
		id[i] = i;
		pthread_create(&tid[i], NULL, t_maior_quadrante, (void*) &id[i]);
	}

	for(i=0; i<lim_threads; i++) pthread_join(tid[i], NULL);

	printf("Maior quadrante 2x2: (%d,%d)\n", maior_quadrante[0], maior_quadrante[1]);

}
void c_soma_total() { 

	int i;

	soma = 0;

	for(i=0; i<num_threads; i++) {
		id[i] = i;
		pthread_create(&tid[i], NULL, t_soma_total, (void*) &id[i]);
	}

	for(i=0; i<num_threads; i++) pthread_join(tid[i], NULL);

	printf("Soma de todos os elementos da matriz: %.2f\n", soma);

}
/* estas threads encontram a coluna que possui a maior soma.Similar a um programa de soma de vetores, cada thread
tem determinadas colunas atribuidas(dependendo do numero de threads e ordem da matriz), realiza a soma e compara
os resultados para descobrir a maior.Como a comparacao exige acesso a uma variavel volatile global, e necessario
o uso de lock */
void *t_maior_soma(void *thread_id) { 

	float soma_coluna = 0;
	int maximo_thread;

	int i,j;

	for(j=0; j<ordem; j++) maximo_thread+= matriz[1][j];

	for(j=*(int*) thread_id; j<ordem; j+=num_threads) {
		soma_coluna = 0;
		for(i=0; i<ordem; i++) {
			soma_coluna += matriz[i][j];
		}
		pthread_mutex_lock(&mutex);
		if(soma_coluna > maximo) {
			maximo = soma_coluna;
			maior_coluna = j;
		}
		pthread_mutex_unlock(&mutex);
	}

	pthread_exit(NULL);
}
/* estas threads varrem a matriz e comparam o valor dos elementos para encontrar o maior */
void *t_maior_elemento(void *thread_id) { 

	float maior_da_thread = matriz[*(int*)thread_id][0];

	int i, j;

	for(i=*(int*) thread_id; i<ordem; i+= num_threads) {
		for(j=0; j<ordem; j++) {
		if(matriz[i][j] > maior_da_thread) maior_da_thread = matriz[i][j];
		}
	}

	pthread_mutex_lock(&mutex);
	if(maior_da_thread > maior_elemento) maior_elemento = maior_da_thread;
	pthread_mutex_unlock(&mutex);

	pthread_exit(NULL);

}
/* cada uma destas threads � atribuida e certos quadrantes.Elas encontram o quadrante com maior soma */
void *t_maior_quadrante(void *thread_id) {

	int i,j,k;

	k = *(int*)thread_id;

	float soma_quadrante = 0, maximo_thread;
	int maior_quad_thread[2];

	maior_quad_thread[0] = k;
	maior_quad_thread[1] = 0;
	maximo_thread = matriz[k][0] + matriz[k][1] + matriz[k+1][0] + matriz[k+1][1];

	for(i=*(int*) thread_id; i<ordem-1; i+=lim_threads) {
		for(j=0; j<ordem-1; j++) {
			soma_quadrante = matriz[i][j] + matriz[i][j+1] + matriz[i+1][j]+matriz[i+1][j+1];
			
			if(soma_quadrante >= maximo_thread) {
				maximo_thread = soma_quadrante;
				maior_quad_thread[0] = i; maior_quad_thread[1] = j;
			}
		}
	}

	pthread_mutex_lock(&mutex);
	if(maximo_thread > maximo_quad) {
		maximo_quad = maximo_thread;
		maior_quadrante[0] = maior_quad_thread[0];
		maior_quadrante[1] = maior_quad_thread[1];
	}
	pthread_mutex_unlock(&mutex);

	pthread_exit(NULL);
			 

}
/* cada thread pega um determinado vetor de elementos da matriz e soma estes vetores.O resultado � lancado na var
global soma atraves de um sistema de lock */
void *t_soma_total(void *thread_id) { 

	int i, j;

	float soma_thread = 0;

	for(i=*(int*) thread_id; i<ordem; i+=num_threads) {
		for(j=0; j<ordem; j++) {
			soma_thread+= matriz[i][j];
		}
	}

	pthread_mutex_lock(&mutex);
	soma += soma_thread;
	pthread_mutex_unlock(&mutex);

	pthread_exit(NULL);
}	

float **matriz;
int ordem;
double inicio, fim, tempo_leitura, tempo_exec;

int main(int argc, char *argv[]) {

	FILE *parq;

	int escolha;

	int i,j;

	/* Abre o arquivo onde est� contida a matriz, v� o tamanho, aloca mem�ria para um vetor bidimensional,
	e preenche esse vetor com os valores da matriz e calcula o tempo gasto */
	GET_TIME(inicio);

	if(argc<2) {
		printf("Chamada incorreta, use o formato: %s <nome do arquivo da matriz>\n", argv[0]);
		exit(-1);
	}

	printf("Vou ler o arquivo da matriz...\n");

	parq = fopen(argv[1], "r");
	if(!parq) {
		printf("Nao consegui encontrar o arquivo %s!\nterminando...!\n", argv[1]);
		exit(-1);
	}

	fscanf(parq, "%d", &ordem);

	matriz = malloc(ordem * sizeof(float*));
	for(i=0; i<ordem; i++) {
		matriz[i] = malloc(ordem * sizeof(float));
	}

	for(i=0; i<ordem; i++) {
		for(j=0; j<ordem; j++) {
			fscanf(parq, "%f", &matriz[i][j]);
		}
	}

	printf("Terminei de ler.\n");

	fclose(parq);
	free(parq);

	GET_TIME(fim);

	tempo_leitura = fim-inicio;
	/* fim do primeiro trecho*/


	/*Aqui o programa pergunta ao usu�rio se deve realizar as opera��es sequencialmente(thread principal) ou
	concorrente(m�ltiplas threads) */
	printf("Qual programa rodar?\n");
	printf(" 1-sequencial\n");
	printf(" 2-concorrente\n");

	scanf("%d", &escolha);
	/*aqui, dependendo do input, o programa faz a chamda da fun��o sequencial() ou concorrente(),
	que executam as operacoes,imprimem resultados e calculam o tempo consumido */
	switch(escolha) {

		case 1: 
			GET_TIME(inicio);
			// sequencial();
			GET_TIME(fim);
			tempo_exec = fim - inicio;
			break;
		case 2: 
			GET_TIME(inicio);
			concorrente();
			GET_TIME(fim);
			tempo_exec = fim - inicio;
			break;
			
		default: 
			printf("escolha invalida!\nterminando...!\n"); 
			exit(-1);
	}

	/*aqui o programa imprime o tempo gasto nos processos e termina*/
	printf("programa executado com sucesso!\n");

	printf("Tempo de Leitura: %.8f\n", tempo_leitura);
	printf("Tempo de execucao: %.8f\n", tempo_exec);
		
	for(i=0; i<ordem; i++) free(matriz[i]);
	free(matriz);
	
	
	return 0;
}