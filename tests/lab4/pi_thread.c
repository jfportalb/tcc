/*
 * Lab 4
 * Calculando pi com threads */

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>

int NUMTHREADS;
int N;
double pi = 0;
double* vetorSoma;

void* incr_pi(void* arg) {
	int ind = *(int*) arg;
	int i;
	double pi_local = 0;
	for (i = ind; i < N; i+=NUMTHREADS) {
		pi += 4*(pow(-1, i) * (1.0/(2*i + 1)));
		pi_local += 4*(pow(-1, i) * (1.0/(2*i + 1)));
	}
	vetorSoma[ind] = pi_local;
	free(arg);
	pthread_exit(NULL);
}

double pi_vetor(double* v, int n) {
	int i;
	double soma = 0;
	for (i = 0; i < n; i++)
		soma += v[i];
	return soma;
}
int main(int argc, char** argv) {
	double tinicio, tfim;
//Leitura da linha de comando. Primeiro o usuario passa o numero de threads e depois o valor de N
	NUMTHREADS = atoi(argv[1]);
	N = atoi(argv[2]);
	vetorSoma = (double*)malloc(NUMTHREADS*sizeof(double));
//--------------------
	pthread_t tid[NUMTHREADS];
	int i;
	int* id;
	for (i = 0; i < NUMTHREADS; i++) {
		id = (int*) malloc(sizeof(int));
		*id = i;
		pthread_create(&tid[i], NULL, incr_pi, (void*)id);
		//sleep(1); Isso conserta o problema de racing
	}

	for (i = 0; i < NUMTHREADS; i++) {
		if (pthread_join(tid[i], NULL)) {
         printf("--ERRO: pthread_join() \n"); exit(-1);
		}
	}
	printf("Tempo do calculo: %f\n", tfim-tinicio);
	printf("Pi calculado: %f\n", pi); // Essa forma é sujeita a problemas
	printf("Pi calculado do vetor soma: %f\n", pi_vetor(vetorSoma, NUMTHREADS)); // Essa forma não. O problema de racing acaba.
	pthread_exit(NULL);

	return 0;
}
