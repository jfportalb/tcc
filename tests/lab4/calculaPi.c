/* Disciplina: Computacao Concorrente */
/* Prof.: Silvana Rossetto */
/* Laboratório: 4 */



#include <stdio.h>
#include <stdlib.h> 
#include <pthread.h>
#include <math.h>

double calculaPi(void);

int NTHREADS;
int TAM;
double valorPi;
double *v;
//cria a estrutura de dados para armazenar os argumentos da thread
typedef struct {
   int idThread, nThreads;
} t_Args;

//funcao executada pelas threads
void *CalcPi (void *arg) {
  t_Args *args = (t_Args *) arg;
  double i;
	v[args->idThread] = 0;
  printf("Sou a thread %d de %d threads\n", args->idThread, args->nThreads);
  for(i=args->idThread;i<TAM;i+=args->nThreads){
		v[args->idThread]+=(pow((-1),i))/((2*i)+1);
	} 
  free(arg);

  pthread_exit(NULL);
}

//funcao principal do programa
int main(int argc, char* argv[]) {
  pthread_t tid_sistema[NTHREADS];
  t_Args *arg; //receberá os argumentos para a thread
	int t;
	valorPi=0;
	double inicio, fim, delta1,delta2;
	
	
	scanf("%d",&NTHREADS);
	scanf("%d", &TAM);
	
	v = (double *) malloc (NTHREADS*sizeof(double));
	if (v == NULL) exit(-1);


  for(t=0; t<NTHREADS; t++) {
    printf("--Aloca e preenche argumentos para thread %d\n", t);
    arg = malloc(sizeof(t_Args));
    if (arg == NULL) {
      printf("--ERRO: malloc()\n"); exit(-1);
    }
    arg->idThread = t; 
    arg->nThreads = NTHREADS; 
    
    printf("--Cria a thread %d\n", t+1);
    if (pthread_create(&tid_sistema[t], NULL, CalcPi, (void*) arg)) {
      printf("--ERRO: pthread_create()\n"); exit(-1);
    }
  }

  //--espera todas as threads terminarem
  for (t=0; t<NTHREADS; t++) {
    if (pthread_join(tid_sistema[t], NULL)) {
         printf("--ERRO: pthread_join() \n"); exit(-1); 
    } 
  }
	for (t = 0; t < NTHREADS; t++) {
		valorPi += v[t];
	}
	// GET_TIME(fim);
	delta1=fim-inicio;

	// GET_TIME(inicio);
	double piSeq =	calculaPi();
	// GET_TIME(fim);
	delta2=fim-inicio;
	
	printf("Pi calculado sequencial : %f \n",piSeq);
	printf("Pi calculado: %f \n",4*valorPi);
	printf("Pi real: %f \n",M_PI);
	printf("Tempo calculo do pi sequencial: %f\n", delta2);
  printf("Tempo calculo do pi com %d threads: %.8lf\n", NTHREADS, delta1);
  //printf("Tempo finalizacoes: %.8lf\n", delta3);
	//printf("Tempo total: %.8lf\n", delta1+delta2+delta3);


  printf("--Thread principal terminou\n");
  pthread_exit(NULL);
}

double calculaPi(void){
double i, pi=0;
for(i=0;i<TAM;i++){
		pi+=(pow((-1),i))/((2*i)+1);
	} 
return 4*pi;
}
