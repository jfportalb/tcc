/*

Lab 4 - Atividade 3

Média de tempos de execução:
+------+------------+-----------+
| n    | 1 thread   | 2 threads |
+------+------------+-----------+
| 10^3 |  0.000305s | 0.000256s |
| 10^4 |  0.001374s | 0.000812s |
| 10^5 |  0.013517s | 0.006820s |
| 10^6 |  0.117021s | 0.064820s |
| 10^7 |  1.154695s | 0.601617s |
| 10^8 | 11.506181s | 6.028967s |
+------+------------+-----------+
*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
// #include "timer.h"

int n; // numero de elementos da sequencia a serem calculados
int n_threads; // numero de threads
double *somas; // vetor de resultado da soma de cada thread

// calcula o elemento i da sequencia
double elemento(int i) {
	return pow(-1.0, i) * (1.0 / (2.0*i + 1.0));
}

// funcao a ser executada por cada thread
void *thread(void *a) {
	int thread_num = *(int *)a; // numero da thread

	int i;
	for(i = thread_num; i < n; i += n_threads) {
		somas[thread_num] += elemento(i);

	}

	free(a);
	pthread_exit(NULL);
}

// abs do math.h do c só recebe int
double abs2(double v) {
	return v > 0 ? v : -v;
}

int main(int argc, char *argv[]) {

	if(argc < 3) {
		printf("Uso: %s <num_elementos> <num_threads>\n", argv[0]);
		exit(0);
	}

	n = atoi(argv[1]); // numero de elementos da seq
	n_threads = atoi(argv[2]); // numero de threads

	somas = malloc(n * sizeof(double));
	pthread_t *tid = malloc(n_threads * sizeof(pthread_t));
	double inicio, fim;

	// GET_TIME(inicio);

	int i;

	for(i = 0; i < n; i++)
		somas[i] = 0;

	for(i = 0; i < n_threads; i++) {
		int *t = malloc(sizeof(int));
		*t = i;

		if(pthread_create(&tid[i], NULL, thread, (void *)t)) {
			printf("Erro: pthread_create()\n");
			exit(-1);
		}
	}

	for(i = 0; i < n_threads; i++) {
		if(pthread_join(tid[i], NULL)) {
			printf("Erro: pthread_join()\n");
			exit(-1);
		}
	}

	double total = 0;
	for(i = 0; i < n_threads; i++)
		total += somas[i];
	total *= 4.0;

	// GET_TIME(fim);


	printf("Pi calculado = %.20lf\n", total);
	printf("Pi \"real\"    = %.20lf\n", M_PI);
	printf("Erro         = %.20lf\n", abs2(total - M_PI));
	printf("Tempo: %lfs\n", fim - inicio);

	pthread_exit(NULL);
	return 0;
}
