/*
* (versão multithreaded)
* Utilização:
* $ ./MT_calcula_pi <tamanho da série> <número de threads>
*/

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int n, m;

void* CalcPI(void* arg){
	int i, index = *(int*) arg;
	int impar = index & 1;
	double pi = 0, *ret;
	
	for(i = index; i < n; i+=m){
		pi += !impar ? 1.0/(2*i+1) : -1.0/(2*i+1);
		if (i & 1)
			impar = m & 1 ? 0 : 1;
		else
			impar = m & 1 ? 1 : 0;
	}

	if (!(ret = malloc(sizeof(double))))
		puts("--ERROR: malloc()"),
		exit(-1);
	*ret = pi;
	
	free(arg);
	pthread_exit((void*) ret);
	return NULL;
}

int main(int argc, char* argv[]){
	int i;
	int* arg;
	double pi = 0;
	double* partial_pi;
	double begin, end, d1, d2, d3;
	pthread_t* tid_system;
	
	
	if (argc > 1){
		n = atoi(argv[1]);
		m = atoi(argv[2]);
	}
	else
		puts("--ERROR: N and/or M were not provided"),
		exit(-1);
	
	if(!(tid_system = malloc(sizeof(pthread_t)*m)))
		puts("--ERROR: malloc()"),
		exit(-1);
		
	
	d1 = end - begin; //tempo de inicialização
	

	for(i = 0; i < m; ++i){
		if (!(arg = malloc(sizeof(int))))
			puts("--ERROR: malloc()"),
			exit(-1);
		*arg = i;
		if(pthread_create((pthread_t*) &tid_system[i],NULL,CalcPI, (void*) arg))
			puts("--ERROR: pthread_create()"),
			exit(-1);
	}
	
	partial_pi = (double*) malloc(sizeof(double));
	for (i = 0; i < m; i++) {
		if (pthread_join(tid_system[i],(void *) &partial_pi))
		    puts("--ERROR: pthread_join()"),
			exit(-1); 
		else
			pi += *partial_pi;
	}
	pi *= 4;
	
	
	d2 = end - begin; //tempo do processamento
	
	
	printf("PI real: %lf\n", M_PI);
	printf("PI aproximado: %lf\n", pi);
	
	free(partial_pi);
	free(tid_system);
	
 	
	d3 = end - begin; //tempo da finalização
	
	printf("\nINPUT: %lfs\n", d1);
	printf("PROCESSAMENTO: %lfs\n", d2);
	printf("FINALIZACAO: %lfs\n", d3);
	
	pthread_exit(NULL);
	return 0;
}
