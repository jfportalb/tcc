#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>

double pi = 0, *vetPi;
int n, nthreads;

void *CalculaPi (void *tid) {
  int i;
  printf("Thread %d iniciou\n", *(int *) tid);
  for(i=*(int *)tid; i<n; i+=nthreads) {
     vetPi[*(int *) tid] += pow(-1, i)*(1./(2*i +1.));;
  }
  printf("Thread %d terminou\n", *(int *)tid);

  pthread_exit(NULL);
}


int main(int argc, char *argv[])
{
	int t, *tid_programa;
	pthread_t *tid_sistema;
	
	if(argc != 3) {
		printf("Use: %s <numero elementos> <numero threads>\n", argv[0]);
    		exit(EXIT_FAILURE);
  	}
	n = atoi(argv[1]);
	nthreads = atoi(argv[2]);

	vetPi = (double *) malloc(sizeof(double) * nthreads);
	if(vetPi==NULL) {
      		printf("--ERRO: malloc()\n"); exit(-1);
  	}

	tid_sistema = (pthread_t *) malloc(sizeof(pthread_t) * nthreads);
	if(tid_sistema==NULL) {
      		printf("--ERRO: malloc()\n"); exit(-1);
  	}

	tid_programa = (int *) malloc(sizeof(int) * nthreads);
 	if(tid_programa==NULL) {
  		printf("--ERRO: malloc()\n"); exit(-1);
 	}

	for(t=0; t<nthreads; t++) {
    		vetPi[t] = 0;
	}

	for(t=0; t<nthreads; t++) {
    		tid_programa[t] = t;
    		if (pthread_create(&tid_sistema[t], NULL, CalculaPi, (void*) &tid_programa[t])) {
     			printf("--ERRO: pthread_create()\n"); exit(-1);
   		}
	}

	for(t=0; t<nthreads; t++) {
     		if (pthread_join(tid_sistema[t], NULL)) {
        		printf("--ERRO: pthread_join()\n"); exit(-1);
    		}
  	}

	for(t=0; t<nthreads; t++) {
    		pi += vetPi[t];
	}

	printf("Meu pi = %.8lf\nM_PI = %.8lf\n", 4*pi, M_PI);
	free(tid_programa);
  	free(tid_sistema);
	pthread_exit(NULL);
	return 0;
}
