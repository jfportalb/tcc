/* Disciplina: Computacao Concorrente */
/* Prof.: Silvana Rossetto */
/* Laboratório: 4 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>



double *vetorGlobal;//Variável global vetor de índices de PI
//--funcao principal do programa
int tam;//Tamanho da precisao de pi desejado
int NTHREADS;//Número de threads

//--funcao executada pelas threads
void *calculaPi (void *arg)
{
    int aux;
    int *indice = (int *) arg;
    double partofpi;
    double aux2;
    double cedula;
    double potencia;
    double dividendo;

    partofpi = 0.0;
    aux2 = *indice;

    for(aux = aux2; aux<tam; aux = aux+=NTHREADS)
    {	
	potencia = pow(-1,(aux));
	dividendo = 2.0*aux + 1.0;
	cedula = 1.0/dividendo;
	partofpi += (double)4.0*potencia*cedula;
    }

    vetorGlobal[*indice]=partofpi;

    pthread_exit(NULL);
}

int main(void)
{
    int t; //variavel auxiliar
    pthread_t *tid_sistema;//Sistema Thread
    int *tids;//Identificador da thread
    double resultado;


    printf("Entre com a precisão de PI.\n");
    scanf("%d",&tam);

    printf("Entre com o número de threads.\n");
    scanf("%d", &NTHREADS);

    vetorGlobal = malloc(sizeof(double)*NTHREADS);
    //TO DO LIST



    //Inicializar vetor global
    for(t=0; t < NTHREADS; t++)
    {
        vetorGlobal[t] = 0;
    }
    puts("\n");


    tid_sistema = malloc(sizeof(pthread_t)*NTHREADS);

    for(t=0; t<NTHREADS; t++)
    {
        tids[t] = t;
        if (pthread_create(&tid_sistema[t], NULL, calculaPi, (void *) &tids[t]))
        {
            printf("--ERRO: pthread_create()\n");
            exit(-1);
        }
    }

//--espera todas as threads terminarem
    for (t=0; t<NTHREADS; t++)
    {
        if (pthread_join(tid_sistema[t], NULL))
        {
            printf("--ERRO: pthread_join() \n");
            exit(-1);
        }
    }

    puts("\n");
    printf("--Thread principal terminou\n");

    puts("Resultado PI");
    resultado = 0.0;
    for(t=0; t<tam; t++)
    {
        resultado += vetorGlobal[t];
    }

    printf("RESULTADO DE PI É: %lf\n",resultado);
    if (resultado==(float)M_PI){
	puts("preciso");
    }
    else
	printf("PI CORRETO VALE: %lf\n",(float)M_PI);
	puts("impreciso");

    pthread_exit(NULL);
}
