#include <stdio.h>
#include <pthread.h>

void* thread ( void* arg ) {
    printf("Hi from the thread\n");

    pthread_exit((void *)NULL);
}

int main ( void ) {
    pthread_t t;
    pthread_create(&t, NULL, thread, NULL);
    printf("Hello from main\n");
    pthread_join(t, NULL);

    pthread_exit((void *)NULL);
}