#include <stdio.h>
#include <pthread.h>

pthread_mutex_t mutex;
int var = 0;

void* thread ( void* arg ) {
    pthread_mutex_lock(&mutex);
    if (var%2)
        var++;
    else
        var += 2;
    pthread_mutex_unlock(&mutex);

    pthread_exit((void *)NULL);
}

int main ( void ) {
    pthread_t t[2]; int i;

    pthread_mutex_init(&mutex, NULL);
    for (i=0; i<2; i++)
        pthread_create(&(t[i]), NULL, thread, NULL);

    pthread_mutex_lock(&mutex);
    var++;
    pthread_mutex_unlock(&mutex);

    for (i=0; i<2; i++)
        pthread_join(t[i], NULL);

    pthread_mutex_destroy(&mutex);
    printf("var = %d\n", var);

    pthread_exit((void *)NULL);
}