#include <stdio.h>
#include <pthread.h>

int var = 0;

void* thread ( void* arg ) {
    var++;

    pthread_exit((void *)NULL);
}

int main ( void ) {
    pthread_t t;
    pthread_create(&t, NULL, thread, NULL);
    var++;
    pthread_join(t, NULL);
    printf("var = %d\n", var);

    pthread_exit((void *)NULL);
}