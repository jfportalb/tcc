#include <stdio.h>
#include <pthread.h>

pthread_mutex_t mutex;
int var;

void* thread ( void* arg ) {
    pthread_mutex_lock(&mutex);
    var++;
    pthread_mutex_unlock(&mutex);

    pthread_exit((void *)NULL);
}

int main ( void ) {
    var = 0;
    pthread_t t;

    pthread_mutex_init(&mutex, NULL);
    pthread_create(&t, NULL, thread, NULL);

    pthread_mutex_lock(&mutex);
    var++;
    pthread_mutex_unlock(&mutex);

    pthread_join(t, NULL);

    pthread_mutex_destroy(&mutex);
    printf("var = %d\n", var);

    pthread_exit((void *)NULL);
}