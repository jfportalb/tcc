#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct
{
    int id;
} thread_arg;

void *thread(void *vargp);

pthread_mutex_t mutex;
int var = 0;

void create_threads(int i){
    pthread_t tid[2];
    thread_arg aa[2];
    aa[i].id = i;
    pthread_create(&(tid[i]), NULL, thread, (void *)&(aa[i]));
    if (i < 1)
        create_threads(i++);
}

int main()
{
    // Cria o mutex
    pthread_mutex_init(&mutex, NULL);

    create_threads(0);

    // Destroi o mutex
    pthread_mutex_destroy(&mutex);

    pthread_exit((void *)NULL);
}

void *thread(void *vargp)
{
    // Converte a estrutura recebida
    thread_arg *a = (thread_arg *) vargp;

    // Como vamos acessar uma variavel global, deve-se protege-la com uma fechadura
    printf("Thread %d: valor de var antes da conta: %d\n", a->id+1, var);
    var = var + a->id + 1;
    printf("Thread %d: valor de var depois da conta: %d\n", a->id+1, var);

    pthread_exit((void *)NULL);
}